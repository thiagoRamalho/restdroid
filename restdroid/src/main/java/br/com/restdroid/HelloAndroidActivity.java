package br.com.restdroid;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class HelloAndroidActivity extends Activity {

	private String pathServer = "https://api.flickr.com/services/rest/?method=flickr.test.echo&name=value";
	
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        textView = (TextView)findViewById(R.id.textview);
        
        new WebClient(pathServer, this).execute();
        
        Toast.makeText(this, "Enviou solicitacao assincrona", Toast.LENGTH_SHORT).show();
        
    }


	public void setText(String result) {
		
		Toast.makeText(this, "Recebendo Resposta", Toast.LENGTH_LONG).show();
		
		this.textView.setText(result);
	}

}

