package br.com.restdroid;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

public class WebClient extends AsyncTask<Void, Void, String>{

	public static final String ACCEPT_XML  = "text/xml";
	public static final String CONTENT_XML = "application/xml";

	private final String url;
	private HelloAndroidActivity activity;

	public WebClient(String url, HelloAndroidActivity activity) {
		this.url = url;
		this.activity = activity;
	}

	private String post(String value, String accept, String contentType) {

		String result = "";

		try {
			Log.d(this.getClass().getSimpleName(), "Inicio");
			DefaultHttpClient httpClient = new DefaultHttpClient();

			HttpPost post = new HttpPost(this.url);

			post.setHeader("Accept", accept);
			post.setHeader("Content-type", contentType);

			post.setEntity(new StringEntity(value));
			
			Log.d(this.getClass().getSimpleName(), "Config");
			
			HttpResponse response = httpClient.execute(post);
			
			Log.d(this.getClass().getSimpleName(), "Resposta");

			result = EntityUtils.toString(response.getEntity());

			Log.d(this.getClass().getSimpleName(), "Finaliza");

			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	public String teste(){

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();

		HttpGet httpGet = new HttpGet(url);

		String text = null;

		try {

			HttpResponse response = httpClient.execute(httpGet, localContext);
			HttpEntity entity = response.getEntity();

			text = EntityUtils.toString(entity);

		} catch (Exception e) {
			e.printStackTrace();
			return e.getLocalizedMessage();
		}

		return text;
	}


	@Override
	protected String doInBackground(Void... params) {
		Log.d(this.getClass().getSimpleName(), "Executando AssyncTask");
		return this.post("<document/>", ACCEPT_XML, CONTENT_XML);
	}
	
	@Override
	protected void onPostExecute(String result) {
		
		Log.d(this.getClass().getSimpleName(), "Resultado: "+result);
		
		super.onPostExecute(result);
		
		this.activity.setText(result);
	}
}